**COMO** (persona o rol de usuario que tiene la necesidad):

**QUIERO** (objetivo, comportamiento, funcionalidad, caracteristica es decir lo que se quiere obtener):

**PARA** (motivo o razón por el que se necesita, valor que se obtiene como resultado):

**Criterio de Aceptación** (condiciones de satisfacción):
